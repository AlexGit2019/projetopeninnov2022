package com.example.projetopeninnov;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Objects;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    TextView bluetoothIsEnabledTextView;
    BluetoothManager bluetoothManager;
    ListView bondedDevicesListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.bluetoothIsEnabledTextView = (TextView)findViewById(R.id.bluetooth_is_enabled);
        /*
            Classe qui permet de faire des actions générales liées au Bluetooth, comme par
            exemple récupérer la liste des appareils connectés, et de récupérer un objet Bluetooth
        */
        bluetoothManager = this.getSystemService(BluetoothManager.class);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        /*
        *   Met un texte en vert qui dit que le Bluetooth est activé
        *   si on lance l'application avec le Bluetooth activé,
        *   Met un texte en rouge qui dit que le Bluetooth est désactivé sinon.
        * */
        if (bluetoothAdapter.isEnabled()) {
            this.bluetoothIsEnabledTextView.setTextColor(Color.GREEN);
            this.bluetoothIsEnabledTextView.setText(this.getString(R.string.bluetooth_state_sumup_string, "activé"));
            //Récupération des appareils associés au téléphone
            Object[] bondedDevices = bluetoothAdapter.getBondedDevices().toArray();
            //Récupération de l'objet ListView qui correspond à l'élément d'interface qui va afficher la liste d'appareils associés
            this.bondedDevicesListView = (ListView) this.findViewById(R.id.bonded_devices);
            // Création d'un nouvel adaptateur et attibution de cet adapteur à la ListView
            this.bondedDevicesListView.setAdapter(new BluetoothDeviceCustomAdapter(this,bondedDevices));
        }
        else {
            this.bluetoothIsEnabledTextView.setTextColor(Color.RED);
            this.bluetoothIsEnabledTextView.setText(this.getString(R.string.bluetooth_state_sumup_string, "désactivé"));
        }
    }
}