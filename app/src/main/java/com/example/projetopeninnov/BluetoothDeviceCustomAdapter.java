package com.example.projetopeninnov;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class BluetoothDeviceCustomAdapter extends android.widget.BaseAdapter{
    private Context context;
    private Object[] bluetoothDeviceArray;
    private LayoutInflater layoutInflater;

    public BluetoothDeviceCustomAdapter(Context context, Object[] bluetoothDeviceArray) {
        this.context = context;
        this.bluetoothDeviceArray = bluetoothDeviceArray;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return bluetoothDeviceArray.length;
    }

    @Override
    public Object getItem(int i) {
        return bluetoothDeviceArray[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = this.layoutInflater.inflate(R.layout.bluetooth_device_view, null);
        // Récupération de l'item qui est à la position i de la liste
        BluetoothDevice currentBluetoothDevice = (BluetoothDevice) this.getItem(i);
        String bluetoothDeviceHardwareAddress = currentBluetoothDevice.getAddress();
        String bluetoothDeviceName = currentBluetoothDevice.getName();

        TextView bluetoothDeviceHardwareAddressTextView = view.findViewById(R.id.hardware_address);
        TextView bluetoothDeviceNameTextView = view.findViewById(R.id.device_name);
        bluetoothDeviceHardwareAddressTextView.setText(bluetoothDeviceHardwareAddress);
        bluetoothDeviceNameTextView.setText(bluetoothDeviceName);
        return view;
    }
}
